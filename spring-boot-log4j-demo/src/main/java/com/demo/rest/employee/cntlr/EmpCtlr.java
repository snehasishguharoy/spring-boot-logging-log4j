package com.demo.rest.employee.cntlr;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rest.employee.model.Employee;

@RestController
public class EmpCtlr {
	private static List<Employee> employees = new ArrayList<>();
	 private static Logger logger = LogManager.getLogger(EmpCtlr.class);

	static {
		employees.add(new Employee("101", "Samik", "Roy"));
		employees.add(new Employee("201", "Tapas", "Roy"));
		employees.add(new Employee("301", "Arpan", "Roy"));
		employees.add(new Employee("401", "Snehasish", "Guha Roy"));

	}

	@GetMapping("/emps")
	public List<Employee> getAllEmps() {
		logger.info("Calling the getAllEmps method ");
		logger.info("Inside the getAllEmps method ");
		return employees;
	}

}
